﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Homework3_Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DessertPage : ContentPage
    {
        List<string> Order_Items = new List<string>();
        double TotalPrice = 0;
        public DessertPage()
        {
            InitializeComponent();
        }
        public void AddItemtoOrder(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            string item = button.Text;
            if (item == "Tiramisu")
            {
                TotalPrice = TotalPrice + 10;
            }
            if (item == "Gelato")
            {
                TotalPrice = TotalPrice + 3.50;
            }
            if (item == "Cannoli")
            {
                TotalPrice = TotalPrice + 5;
            }
            if (item == "Chocolate Lava Cake")
            {
                TotalPrice = TotalPrice + 8;
            }
            Order_Items.Add(item);

        }
        public void DisplayOrder(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            string item = button.Text;
            
            Order_Label.Text = string.Join("\n ", Order_Items);
            string Total = TotalPrice.ToString(); 
            Price_Label.Text = Total;
        }
        async void Handle_Appearing(object sender, EventArgs e)
        {

            await Task.Delay(200);
            this.BackgroundColor = Color.Pink;
            await base.DisplayAlert("Order Your Dessert(s)!", "Click on the button under the item of the same name to add it to your order.", "Close");
        }
        async void Handle_Disappearing(object sender, EventArgs e)
        {
            await Task.Delay(200);
            this.BackgroundColor = Color.Purple;
            Order_Label.Text = string.Join("\n ", "");
            Order_Items = new List<string>();
            Order_Label.Text = string.Join("\n ", "");
            TotalPrice = 0;
        }

    }
}