﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Homework3_Tabbed
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            var T_Page = new TabbedPage();
            T_Page.Children.Add(new MainPage());
            T_Page.Children.Add(new DrinkPage());
            T_Page.Children.Add(new EntreePage());
            T_Page.Children.Add(new DessertPage());
            //T_Page.Children.Add(new OrderPage());
            MainPage = new TabbedPage();
            MainPage = T_Page;
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
