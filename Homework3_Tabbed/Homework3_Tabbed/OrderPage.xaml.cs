﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Homework3_Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderPage : ContentPage
    {
        public OrderPage()
        {
            InitializeComponent();
            this.BackgroundColor = Color.Gold;
        }
        async void Handle_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(200);
            
        }
        async void Handle_Disappearing(object sender, EventArgs e)
        {
            await Task.Delay(200);
            
        }
    }
}