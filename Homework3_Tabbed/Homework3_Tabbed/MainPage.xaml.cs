﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Homework3_Tabbed
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {

        public MainPage()
        {
            this.BackgroundImage = "italy1.jpg";
            InitializeComponent();
        }
        async void Handle_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(1000);
            this.BackgroundImage = "florence.jpg";
        }
        async void Handle_Disappearing(object sender, EventArgs e)
        {
            await Task.Delay(1000);
            this.BackgroundImage = "venice.jpg";
        }
    }
}
