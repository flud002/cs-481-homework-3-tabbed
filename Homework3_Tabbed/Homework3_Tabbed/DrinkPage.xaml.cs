﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Homework3_Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DrinkPage : ContentPage
    {
        List<string> Order_Items = new List<string>();
        double TotalPrice = 0;
        public DrinkPage()
        {
            InitializeComponent();
        }
        public void AddItemtoOrder(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            string item = button.Text;
            if(item == "water")
            {
                TotalPrice = TotalPrice + 0;
            }
            if(item == "Red Wine(Glass)")
            {
                TotalPrice = TotalPrice + 8;
            }
            if (item == "Red Wine(Bottle)")
            {
                TotalPrice = TotalPrice + 30;
            }
            if (item == "White Wine(Glass)")
            {
                TotalPrice = TotalPrice + 6;
            }
            if (item == "White Wine(Bottle)")
            {
                TotalPrice = TotalPrice + 25;
            }
            if (item == "Moscow Mule")
            {
                TotalPrice = TotalPrice + 8;
            }
            Order_Items.Add(item);
        }
        public void DisplayOrder(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            string item = button.Text;

            Order_Label.Text = string.Join("\n ", Order_Items);
            string Total = TotalPrice.ToString();
            Price_Label.Text = Total;
            
        }
        async void Handle_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(200);
            this.BackgroundColor = Color.SkyBlue;
            await base.DisplayAlert("Order Your Drink(s)!","Click on the button under the item of the same name to add it to your order.", "Close");
        }
        async void Handle_Disappearing(object sender, EventArgs e)
        {
            await Task.Delay(200);
            this.BackgroundColor = Color.LightCoral;
            Order_Label.Text = string.Join("\n ", "");
            Price_Label.Text = string.Join("\n ", "");
            Order_Items = new List<string>();
            TotalPrice = 0;
        }
    }
}