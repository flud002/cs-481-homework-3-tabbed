﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Homework3_Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EntreePage : ContentPage
    {
        List<string> Order_Items = new List<string>();
        double TotalPrice = 0;
        public EntreePage()
        {
            InitializeComponent();
        }
        public void AddItemtoOrder(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            string item = button.Text;
            if (item == "Filet Mingon")
            {
                TotalPrice = TotalPrice + 40;
            }
            if (item == "Pork Chop")
            {
                TotalPrice = TotalPrice + 20;
            }
            if (item == "Maine Lobster")
            {
                TotalPrice = TotalPrice + 30;
            }
            if (item == "Salmon")
            {
                TotalPrice = TotalPrice + 25;
            }
            Order_Items.Add(item);
        }
        public void DisplayOrder(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            string item = button.Text;

            Order_Label.Text = string.Join("\n ", Order_Items);
            string Total = TotalPrice.ToString();
            Price_Label.Text = Total;
           
        }
        async void Handle_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(200);
            this.BackgroundColor = Color.Orange;
            await base.DisplayAlert("Order Your Entree(s)!", "Click on the button under the item of the same name to add it to your order.", "Close");
        }
        async void Handle_Disappearing(object sender, EventArgs e)
        {
            await Task.Delay(200);
            this.BackgroundColor = Color.Yellow;
            Order_Label.Text = string.Join("\n ", "");
            Order_Items = new List<string>();
            Price_Label.Text = string.Join("\n ", "");
            TotalPrice = 0;
        }
    }
}